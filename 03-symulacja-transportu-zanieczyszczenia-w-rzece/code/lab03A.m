close all;
clear;
clc;

t = 2000; % s
length = 100; % m
width = 5; % m
depth = 1; % m

xi = 10; % m
xe = 21; % m

m = 2.8; % kg
dx = 0.17;
dt = 0.27;

i_length = ceil(length/dx);
i_xi = ceil(xi/dx);
i_xe = ceil(xe/dx);

N = ceil(t/dt);

U = 0.03; % [m/s] predkosc wody
D = 0.01; % [m^2/s] wspolczynnik dyspersji

ca = (U*dt)/ dx; %adwekcyjna liczba Couranta
cd = (D*dt)/(dx*dx); %dyfuzyjna liczba Couranta

timesOfMeasurement = 4;

for k = [8]
    CC = zeros(1,i_length);
    C = zeros(1,i_length);
    CforXe = zeros(1, N);
    iter = 1;
    while iter <= N
        C = CC;
        if iter <= k 
            C(i_xi) = C(i_xi) + (m/(width*depth*dx))/k;
        end
        for j = 3:i_length-2
           CC(j) = C(j)...
            + (cd*(1-ca)-(ca/6.0)*(ca^2-3*ca+2))*C(j+1)...
            -(cd*(2-3*ca)-(ca/2.0)*(ca^2-2*ca-1))*C(j)...
            +(cd*(1-3*ca)-(ca/2.0)*(ca^2-ca-2))*C(j-1)...
            +(cd*ca+(ca/6.0)*(ca^2-1))*C(j-2);
        end
        if mod(iter, N/timesOfMeasurement-1) == 0
            sum = 0;
            for el = CC
                sum = sum + el;
            end
            sum = sum * width * depth * dx;
            disp("" + iter + ": " + sum);
            figure;
            plot(CC);
            %saveas(plot(CC), "A_CXT_" + U + "-U_" + D + "-D_" + iter + "-ofIters_" + N + "-N_" + k + "-k.png");
        end
        CforXe(iter) = CC(i_xe);
        iter = iter + 1;
    end
    sum = 0;
    for el = CforXe
        sum = sum + el;
    end
    sum = sum * dt;
    % Brak sensu fizycznego
    disp("CforXe: " + sum);
    disp(sum);
    figure;
    plot(CforXe);
    % saveas(plot(CforXe), "A_CL1T_" + k + "-k.png");
end