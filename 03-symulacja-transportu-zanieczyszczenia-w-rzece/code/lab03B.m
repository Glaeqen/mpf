U = 0.1;
D = 0.2;
m = 1.8; % kg

dx = 0.2;
dy = 0.2;
dt = 0.05;
k = 4;

dlugosc = 100; % m
szerokosc = 10; % m
xi = 50; % m 
xe = 90; % m 


T = 2000;
N = 1000;
t = 0;

Ux = U;
Uy = 0;

% a)
X = zeros(1,N);
prevX = zeros(1,N);
X(:) = xi;
Y = rand(1,N)*szerokosc;
alfaL = D*(U*(xe-xi));
alfaT = 0.15;

CforXe = zeros(1,T);

iter = 0;
while iter < T
    prevX = X;
    for j = 1 : N
        R1 = rand();
        R2 = rand();
        X(j) = X(j) + Ux * dt + R1 * sqrt(2*alfaL*U*dt) * (Ux/U);
        y = Y(j) + Uy * dt + R2 * sqrt(2*alfaT*U*dt) * (Ux/U);
        if y > szerokosc
            Y(j) = szerokosc;
        elseif y < 0
            Y(j) = 0;
        else
            Y(j) = y;
        end
    end
    iter = iter + 1;
    
    for i = 1:N
        if (prevX(i) <= xe && xe <= X(i)) || (X(i) <= xe && xe <= prevX(i))
            CforXe(iter) = CforXe(iter) + 1;
        end
    end
    
 %   plot(X, Y, '.');
 %   xlim([0 dlugosc]);
 %   ylim([0 szerokosc]);
 %   pause(1);
end

plot(CforXe);

% b)

X = zeros(1,N);
X(:) = dx*xi;
Y = zeros(1,N);

alfaL = D*(U*(xe-xi));
alfaT = 0.15;

iter = 0;
while iter < T
    if iter > N
        n = N;
    else
        n = iter*4;
    end
    for j = 1 : n
        R1 = randn();
        R2 = randn();
        X(j) = X(j) + Ux * dt + R1 * sqrt(2*alfaL*U*dt) * (Ux/U);
        y = Y(j) + Uy * dt + R2 * sqrt(2*alfaT*U*dt) * (Ux/U);
        if y > szerokosc
            Y(j) = szerokosc;
        elseif y < 0
            Y(j) = 0;
        else
            Y(j) = y;
        end
        
    end
    iter = iter + 1;
    
%    plot(X, Y, '.');
%    xlim([0 dlugosc]);
%    ylim([0 szerokosc]);
%    pause(0.1);
end
