% Ilosc punktow
N = 1000;
% Kroki czasowe
L = 10000;

dx = randn(L,N);
dy = randn(L,N);

x = cumsum(dx);
y = cumsum(dy);
plot(x,y);

t = 1:L;

figure
plot(t, sum(x.^2 + y.^2, 2)/ N )

X = [x(end,:)', y(end,:)'];
figure
hist3(X, 'Nbins', [30,30] )
