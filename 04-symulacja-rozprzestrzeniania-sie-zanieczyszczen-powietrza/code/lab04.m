close all;
clear;
clc;
% m, a, b  % stan rownowagi atmosfery
% U        % srednia predkosc wiatru [m/s]

%             m     a     b     U (maximum values of U per state)
params_set = [0.080 0.888 1.284 3;
              0.143 0.865 1.108 5;
              0.196 0.845 0.978 8;
              0.270 0.818 0.822 11;
              0.363 0.784 0.660 5;
              0.440 0.756 0.551 4];
    
iter = 1;
for params = transpose(params_set)
  m = params(1);
  a = params(2);
  b = params(3);
  U = params(4);
  % Substancje gazowe
  Sgaz = sim(m, a, b, U, @(E_g, E_p, pi, U,sigma_z, sigma_y) E_g/(pi*U* sigma_z* sigma_y));
  figure1 = figure;
    axes1 = axes('Parent',figure1);
    hold(axes1,'on');
    surf(Sgaz,'Parent',axes1);
    view(axes1,[108.599997724561 59.9999998448574]);
    grid(axes1,'on');
  saveas(figure1, "gaz-" + iter + "-" + m + "-" + a + "-" + b + "-" + U + ".png"); 

  % Pyl zawieszony
  Spyl = sim(m, a, b, U, @(E_g, E_p, pi, U,sigma_z, sigma_y) E_p/(2*pi*U* sigma_z* sigma_y));
    figure1 = figure;
    axes1 = axes('Parent',figure1);
    hold(axes1,'on');
    surf(Spyl,'Parent',axes1);
    view(axes1,[108.599997724561 59.9999998448574]);
    grid(axes1,'on');
  saveas(figure1, "pyl-" + iter + "-" + m + "-" + a + "-" + b + "-" + U + ".png");
  iter = iter + 1;
end

close all;


function S = sim(m, a, b, U, sim_fun)
    H = 10; % efektywna wysokosc emitora

    %E = BxW
    B = 5.2e6/3600; % zuzycie paliwa 5,2 kg na godzine
    W_pyl = 1500*8; % wskaznik emisji pylu * 8% popiolu
    W_benzopiren = 14;

    E_p =  B * W_pyl; % maksymalna emisja pylu zawieszonego [mg/s] 
    E_g =  B * W_benzopiren; % maksymalna emisja substancji gazowej [mg/s]

    z_0 = 0.02; % srednia wartosc wspolczynnika aerodynamicznej szorstkosci terenu 

    A = 0.088 * ( 6.0 * ( m^(-0.3) ) + 1.0 - log(H/z_0)); % wspolczynnik poziomej dyfuzji atmosferycznej
    B = (0.38*m^(1.3))* (8.7 - log(H/z_0)) ; % wspolczynnik pionowej dyfuzji atmosferycznej

    size_x = 45; % rozmiar dla osi X macierzy, jeden element macierzy ma fizyczny rozmiar square_size
    size_y = 45; % rozmiar dla osi Y macierzy
    wind_angle = 22.5 + 90; % [stopnie]
    % os X ma reprezentowac kierunek wiatru stad zamiast isc w prawo, 
    % idzie w NNE, stad taki obrot 
    square_size = 5; % fizyczny rozmiar pojedynczego bloczka w macierzy [m]
    
    % Zmiana 25m bloczek * 9 el ze wzgledu na wiecej informacji w
    % histogramie

    komin_pos = [size_x; 1]; % pozycja komina na macierzy

    %S = %ug/m3 stezenie substancji w powietrzu
    S = zeros(size_x, size_y);

    for y_iter = 1:size_y
        for x_iter = 1:size_x
            relative_pos = [x_iter; y_iter] - komin_pos; % Traktowanie komin_pos jako punkt (0,0)
            
            relative_pos(2) = -relative_pos(2); % Negacja wartosci y 
                                                % ^- w macierzy y rosnie odwrotnie jak w ukladzie wspolrzednych
                                                
            relative_pos = relative_pos * square_size; % wektor odleglosci w fizycznych jednostkach
            
            % obrocenie ukladu wspolrzednych tak, ze os X stanowi kierunek wiatru
            relative_pos_rotated_to_wind = [cosd(wind_angle) -sind(wind_angle); sind(wind_angle) cosd(wind_angle)]*relative_pos;
            
            % x = skladowa odleglosci emitora od punktu dla ktorego dokonuje sie obliczen - rownolegla do kierunku wiatru
            x = relative_pos_rotated_to_wind(1);
            % y = skladowa odleglosci emitora od punktu dla ktorego dokonuje sie obliczen - prostopadla do kierunku wiatru
            y = relative_pos_rotated_to_wind(2);
            if(x <= 0) % jesli liczymy pod wiatr za emiterem lub w miejscu kominow 
                       % to wymuszamy zero
                       % poniewaz algorytm generuje liczby zespolone przez
                       % x < 0 && (a < 1 || b < 1)
                S(x_iter, y_iter) = 0;
            else
                sigma_y = A*(x^a); 
                sigma_z = B*(x^b);
                S(x_iter, y_iter) = sim_fun(E_g, E_p, pi, U, sigma_z, sigma_y) * ...
                                    exp(-(y^2)/(2*sigma_y^2)) * ...
                                    exp(-(H^2)/(2*sigma_z^2)) * 1000;
            end
        end
    end
end