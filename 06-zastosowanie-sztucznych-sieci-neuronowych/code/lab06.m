clear;
close all;
clc;
for algorithm = ["trainlm", "trainbr", "trainscg"]
    for tryt = [80]
        current_image = strcat(algorithm, '-', num2str(tryt), '.png');
        b = importdata(strcat('tryt_', num2str(tryt), '.dat'));
        b = transpose(b);
        output = b(1,:);
        input = b(2:end,:);
        
        net = fitnet(10, algorithm);       
        net.divideParam.trainRatio = 70/100;
        net.divideParam.valRatio = 15/100;
        net.divideParam.testRatio = 15/100;

        if(algorithm == "trainbr")
            net.trainParam.epochs = 25;
        end

        net = train(net, input, output);
        pause(5);
        %sprawdzanie poprawnosci nauczenia sieci

        close all;
    end
end