clear;
clc;

a = 0.2;
b = 0.04;
h = 0.001;
kappa = 1.1*10e-4;
lambda = 401;
boltz = 1.38064852 * 10e-23;

dx = 0.005;
dy = 0.005;
dt = 0.001;

Na = a/dx+1;
Nb = b/dx+1;
brzeg = (Na-Nb)/2+1;

T= 20*ones(Na);
TT = T;
TT(brzeg:brzeg+Nb-1,brzeg:brzeg+Nb-1) = 80;


while max(max(abs(T-TT)))>dt
    T=TT;
    for i =2:Na-1
        for j = 2:Na-1
            TT(i,j) = T(i,j) + (kappa * dt / (dx*dx)) * (T(i+1,j) - 2*T(i,j) + T(i-1,j)) + ...
                                   (kappa * dt / (dy*dy)) * (T(i,j+1) - 2*T(i,j) + T(i,j-1));
        end
    end
    TT(brzeg+1:Na-brzeg,brzeg+1:Na-brzeg) = 80;
    TT(1,:) = TT(2,:);
    TT(Na,:) = TT(Na-1,:);
    TT(:,1) = TT(:,2);
    TT(:,Na) = TT(:,Na-1);
    surf(TT);
    pause(0.01);
end
%%%
